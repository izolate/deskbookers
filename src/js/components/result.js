import React, { Component } from 'react'

class Result extends Component {
  constructor () {
    super()
  }

  get rating () {
    if (!this.props.rating) return 'N/A'
    else return parseFloat(this.props.rating).toFixed(1)
  }

  render () {
    return (
      <article className='result'>
        <header>
          <img src={this.props.image} />
          <span className='result__price'>
            <span className='result__currency'>€</span>
            {this.props.price}
            <sup>/hr</sup>
          </span>
        </header>
        <section>
          <h2>{this.props.title.substr(0, 30)}</h2>
          <div className='result__meta'>
            <span className='result__rating'>{this.rating}</span>
            <span className='result__location'>{this.props.location.substr(0,30)}</span>
          </div>
        </section>
      </article>
    )
  }
}

export default Result
