import React, { Component } from 'react'
import Search from './search'
import id from 'shortid'

class Slider extends Component {
  constructor () {
    super()
    this.state = {
      activeSlide: 0,
      slides: [
        'First slide',
        'Second slide',
        'Third slide'
      ]
    }

    // Re-flow slide on window resize
    window.addEventListener('resize', e => this.changeSlide())
  }

  get css () {
    return {
      sliderContainer: {
        height: `${this.state.slides.length * 100}vh`,
        transform: `translateY(${-this.state.activeSlide * (window.innerHeight-65)}px)`
      }
    }
  }

  changeSlide (e) {
    const slide = e ? parseInt(e.target.dataset.slide) : this.state.activeSlide
    this.setState({ activeSlide: slide })
    this.forceUpdate()
  }

  render () {
    return (
      <section className='slider'>
        <div className='slider__container' style={this.css.sliderContainer}>
          {this.state.slides.map((slide, i) => {
            return (
              <div
                className='slide'
                key={id.generate()}
                style={{backgroundImage: `url('src/img/slide-${i}.jpg')`}}>
                <h2>{slide}</h2>
              </div>
            )
          })}
        </div>
        <nav className='slider__nav'>
          {this.state.slides.map((slide, i) => {
            return (
              <button
                key={id.generate()}
                className={(this.state.activeSlide === i? 'active': '')}
                onClick={this.changeSlide.bind(this)}
                data-slide={i} />
            )
          })}
        </nav>
        <Search handleSearch={this.props.handleSearch} />
      </section>
    )
  }
}

export default Slider
