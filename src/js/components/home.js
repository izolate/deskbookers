import React, { Component } from 'react'
import id from 'shortid'
import Slider from './slider'
import Result from './result'
import sweetScroll from 'sweet-scroll'

class Home extends Component {
  constructor () {
    super()
    this.sweetScroll = new sweetScroll()
    this.state = {
      searchResults: []
    }
  }

  updateSearchResults (searchResults) {
    this.setState({ searchResults })
    this.forceUpdate()

    if (!searchResults.length)
      alert(`Sorry, your search didn't yield any results.`)
  }

  get resultsClassName () {
    const base = 'results'
    if (this.state.searchResults.length) return base
    else return `${base} hidden`
  }

  componentDidUpdate () {
    if (this.state.searchResults.length)
      this.sweetScroll.toTop(window.innerHeight)
  }

  render () {
    return (
      <div>
        <header className='navbar'>
          <img src='src/img/logo.png' alt='Deskbookers Logo' />
        </header>
        <Slider handleSearch={this.updateSearchResults.bind(this)} />
        <section className={this.resultsClassName}>
          {this.state.searchResults.map((item, i) => {
            return <Result
              title={item.name}
              location={item.location_name}
              price={item.hour_price}
              rating={item.rating}
              image={item.image_urls2[0]}
              key={id.generate()} />
          })}
        </section>
      </div>
    )
  }
}

export default Home
