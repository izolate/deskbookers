import React, { Component } from 'react'
import id from 'shortid'

class Search extends Component {
  constructor () {
    super()
    this.state = {
      searchKeyword: null
    }
  }

  getApiUrl (keyword) {
    return `https://www.deskbookers.com/nl-nl/sajax.json?q=${encodeURIComponent(keyword)}`
  }

  handleInputChange (e) {
    this.setState({ searchKeyword: e.target.value })
  }

  handleSubmit (e) {
    e.preventDefault()

    // Query API
    fetch(this.getApiUrl(this.state.searchKeyword))
      .then(resp => resp.json())
      .then(body => this.props.handleSearch(body.rows))
  }

  render () {
    return (
      <form
        className='search'
        onSubmit={this.handleSubmit.bind(this)}>
        <input
          type='text'
          onChange={this.handleInputChange.bind(this)}
          placeholder='Find desks in your city' />
        <button type='submit' />
      </form>
    )
  }
}

export default Search
