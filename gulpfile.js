'use strict'

const gulp = require('gulp')
const tasks = ['js', 'css']

require('require-dir')('./tasks', { recurse: true })

gulp.task('develop', tasks.concat(['watch']))
gulp.task('build', tasks)
