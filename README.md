# Deskbookers Frontend Challenge

Requirements: https://github.com/deskbookers/frontend-test

## Installation

Clone Repo: `git clone git@gitlab.com:izolate/deskbookers.git`

```bash
# Install dependencies
npm install

# Run tasks
npm run build # production
gulp develop # development
```

Open `index.html`
