const gulp = require('gulp')

gulp.task('watch', () => {
  gulp.watch('src/css/*.sss', ['css'])
  gulp.watch('src/js/**/*.js', ['js'])
})
