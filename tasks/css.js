const gulp = require('gulp')
const _ = require('gulp-load-plugins')()
const cssnext = require('postcss-cssnext')
const sugarss = require('sugarss')
const precss = require('precss')
const easyImport = require('postcss-easy-import')

gulp.task('css', () => {
  return gulp.src('src/css/index.sss')
    .pipe(_.sourcemaps.init())
    .pipe(_.postcss([
      precss, cssnext, easyImport({ extensions: ['.sss'] })
    ], { parser: sugarss }))
    .pipe(_.cssnano())
    .pipe(_.rename({ extname: '.css' }))
    .pipe(_.sourcemaps.write('.'))
    .pipe(gulp.dest('dist'))
})
